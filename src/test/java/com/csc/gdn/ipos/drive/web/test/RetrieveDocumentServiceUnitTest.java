package com.csc.gdn.ipos.drive.web.test;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.csc.gdn.ipos.core.profile.RequestProfile;
import com.csc.gdn.ipos.core.service.controller.IServiceController;
import com.csc.gdn.ipos.core.service.message.Message;
import com.csc.gdn.ipos.core.service.message.MessageHelper;
import com.csc.gdn.ipos.drive.api.model.DriveRequest;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/META-INF/test/ipos-drive-web-context-test.xml")
public class RetrieveDocumentServiceUnitTest
{		
	@Resource 
	protected MessageHelper messageHelper;
	
	@Resource
	protected IServiceController driveManager;
	
	
	@Test
	public void testRetriveDocument() throws Exception
    {
		DriveRequest driveRequest = new DriveRequest();
		driveRequest.setUserName("test@liferay.com");
		driveRequest.setPassword("test");
		driveRequest.setGroupId("10184");
		driveRequest.setUuid("6f071cba-b287-4b13-a248-3ef40b4b7b4c");
		
/*		RequestProfile requestProfile = new RequestProfile();
		requestProfile.setServiceName("retrieveDocumentByUUID");
		
		Message request = messageHelper.newMessage(requestProfile);
		messageHelper.putParam2(request, driveRequest);		
		
		System.out.println(driveManager.process(request));
*/		
    }
		
}
