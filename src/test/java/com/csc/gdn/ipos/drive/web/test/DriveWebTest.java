package com.csc.gdn.ipos.drive.web.test;

import java.io.File;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.csc.gdn.ipos.drive.api.DriveConstants;
import com.csc.gdn.ipos.drive.api.impl.helper.SecurityHelper;
import com.google.gson.Gson;

public class DriveWebTest {
	private String host = "localhost";
	private int port = 8080;
	
	public static void main(String[] args) throws Exception {
		DriveWebTest driveWebTest = new DriveWebTest();
		
		//get session
		String token = driveWebTest.getSession();
		System.out.println("token: " + token);
		
		//save file
		String result = driveWebTest.addFileEntry(token);
		System.out.println("result: " + result);
	}
	private URIBuilder defaultUriBuilder() {
		return new URIBuilder().setScheme("http").setHost(host).setPort(port);
	}
	
	public void setPostRequestHeader(HttpPost postRequest, Map<String, String> headerMap){
		String sessionToken = headerMap.get(DriveConstants.HEADER_SESSION_TOKEN);
		postRequest.addHeader(DriveConstants.HEADER_SESSION_TOKEN, sessionToken);
		String urlStatic = "";
		urlStatic = headerMap.get(DriveConstants.HEADER_URL_STATIC);
		urlStatic = urlStatic.substring(1, urlStatic.length());
		SecurityHelper securityHelper = new SecurityHelper();
		
		postRequest.addHeader(DriveConstants.HEADER_CSRF_TOKEN, securityHelper.encryptByStringKey(sessionToken + urlStatic));
	}
	
	public String getSession() throws Exception {
		String encryptedUsername =  "X8jTF6LuYHByFgTamMrTRD8TAApagNQF5kobT+3lm3E=";
		String encryptedPassword = "5G8rP0Mm/HZRnwHxdcA+OA==";
		MultipartEntityBuilder builder = MultipartEntityBuilder.create();
		
		builder.addTextBody("encryptedUsername", encryptedUsername);
		builder.addTextBody("encryptedPassword", encryptedPassword);
		
		URI uri = defaultUriBuilder().setPath("/ipos-drive-web/drive/session").build();
		HttpPost postRequest = new HttpPost(uri);
		postRequest.setEntity(builder.build());

		Map<String, String> headerMap = new HashMap<String, String>();
		String randomToken = String.valueOf(System.currentTimeMillis());
		
		headerMap.put(DriveConstants.HEADER_SESSION_TOKEN, Base64.encodeBase64String(randomToken.getBytes()));
		headerMap.put(DriveConstants.HEADER_URL_STATIC, DriveConstants.COMMAND_URL_DRIVE_SESSION);

		setPostRequestHeader(postRequest, headerMap);
		
		String dmsResultString = doRequest(postRequest);
		
		JSONParser parser=new JSONParser();					
		Object object = parser.parse(dmsResultString);
		JSONObject jsonObject = (JSONObject)object;
		
		Object result = jsonObject.get("result");
		
		return (String) result;
	}
	
	public String addFileEntry(String token) throws Exception {		
		String folderId = "43438";
		String description = "description prospect-example";
		String changeLog = "change log";
		boolean approved = true;
		
		List<Map<String, String>> listMap = new ArrayList<Map<String,String>>();
		Map<String, String> metadataMapCommon = new HashMap<String, String>();
		metadataMapCommon.put("DocType", "prospect");
		metadataMapCommon.put("Occupation", "AVAV");
		metadataMapCommon.put("FirstName", "iPos_firstName_demo_file");
		metadataMapCommon.put("LastName", "iPos_lastName_demo_file");
		listMap.add(metadataMapCommon);
		
		Gson gson = new Gson();
		String jsonMetaData = gson.toJson(metadataMapCommon);

		File file = new File("D:\\file.pxml");				
		MultipartEntityBuilder builder = MultipartEntityBuilder.create();
			
		builder.addTextBody("d_auth", token);
		builder.addTextBody("parentFolderId", folderId);
		builder.addTextBody("description", description);
		builder.addTextBody("changeLog", changeLog);
		builder.addTextBody("jsonMetaData", jsonMetaData);
		builder.addTextBody("approved", String.valueOf(approved));
		builder.addPart("file", new FileBody(file));

		
		URI uri = defaultUriBuilder().setPath("/ipos-drive-web/drive/save/meta").build();
		HttpPost postRequest = new HttpPost(uri);
		postRequest.setEntity(builder.build());

		Map<String, String> headerMap = new HashMap<String, String>();
		headerMap.put(DriveConstants.HEADER_SESSION_TOKEN, token);
		headerMap.put(DriveConstants.HEADER_URL_STATIC, DriveConstants.COMMAND_URL_SAVE_DOCUMENT_META);
		setPostRequestHeader(postRequest, headerMap);
		
		String dmsResultString = doRequest(postRequest);
		
		return dmsResultString;
	}
	
	public String doRequest(HttpPost request) throws Exception {		 
		HttpClient client = HttpClientBuilder.create().build();
		
		HttpResponse response = client.execute(request);
		HttpEntity entity = response.getEntity();
		
		String result = null;
		if (entity != null) {
			result = new String(IOUtils.toByteArray(entity.getContent()));
		}
	
		return result;
	}
}
