package com.csc.gdn.ipos.drive.web.test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import org.junit.Assert;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MvcResult;

import com.csc.gdn.ipos.drive.api.model.DriveRequest;
import com.csc.gdn.ipos.drive.api.model.OldDriveResponse;


public class RetrieveDocumentListServiceUnitTest extends AbstractDriveTest{

//	@Test
	public void testRetrieveDocumentList(){
		MvcResult mvcResult;
		try {
			DriveRequest driveRequest = new DriveRequest();
			
			driveRequest.setP_auth("bja4r64g");
			driveRequest.setRepositoryId("10184");
			driveRequest.setParentFolderId("0");
			
			mvcResult = this.mockMvc.perform(post("/drive/documentList").contentType(MediaType.APPLICATION_FORM_URLENCODED).sessionAttr("driveRequest", driveRequest).accept(MediaType.APPLICATION_JSON)).andReturn();
			MockHttpServletResponse response = mvcResult.getResponse();
			int status =  response.getStatus();
			Assert.assertEquals(200, status);
			String content = mvcResult.getResponse().getContentAsString();
			OldDriveResponse result = jsonObjectMapper.readValue(content, OldDriveResponse.class);
			Assert.assertTrue(result != null);
			System.out.println(result.getResult());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
//	@Test
//	public void testRetrieveDocumentLists(){
//		try {
//			DriveRequest driveRequest = new DriveRequest();
//			
//			driveRequest.setP_auth("bja4r64g");
//			driveRequest.setRepositoryId("10184");
//			driveRequest.setParentFolderId("0");
//			
//			List<NameValuePair> params = new ArrayList<NameValuePair>();
//			
//			params.add(new BasicNameValuePair("repositoryId", repositoryId));
//			
//			URI uri = repositoryHelper.defaultUriBuilder().setPath("/api/jsonws/dlapp/add-folder")
//					.setParameters(params).build();
//			HttpGet requestGet = new HttpGet(uri);
//			
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
	
	public static byte[] toByteArray(Object obj) throws IOException {
        byte[] bytes = null;
        ByteArrayOutputStream bos = null;
        ObjectOutputStream oos = null;
        try {
            bos = new ByteArrayOutputStream();
            oos = new ObjectOutputStream(bos);
            oos.writeObject(obj);
            oos.flush();
            bytes = bos.toByteArray();
        } finally {
            if (oos != null) {
                oos.close();
            }
            if (bos != null) {
                bos.close();
            }
        }
        return bytes;
    }
}
