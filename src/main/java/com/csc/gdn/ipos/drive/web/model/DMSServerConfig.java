package com.csc.gdn.ipos.drive.web.model;


public class DMSServerConfig{

	private String host;
	private int port;
	private String userName;
	private String password;
	private String driveClass;
	
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getDriveClass() {
		return driveClass;
	}
	public void setDriveClass(String driveClass) {
		this.driveClass = driveClass;
	}
	
	
}
