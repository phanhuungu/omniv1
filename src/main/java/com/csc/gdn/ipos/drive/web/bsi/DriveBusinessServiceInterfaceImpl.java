package com.csc.gdn.ipos.drive.web.bsi;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.springframework.beans.factory.annotation.Autowired;

import com.csc.gdn.ipos.core.exception.ServiceProcessException;
import com.csc.gdn.ipos.core.profile.RequestProfile;
import com.csc.gdn.ipos.drive.api.audit.model.AuditData;
import com.csc.gdn.ipos.drive.api.audit.model.ProcessAuditData;
import com.csc.gdn.ipos.drive.api.audit.model.TaskAuditData;
import com.csc.gdn.ipos.drive.api.audit.model.UserAuditData;
import com.csc.gdn.ipos.drive.api.device.model.AppDevice;
import com.csc.gdn.ipos.drive.api.device.model.AppNotification;
import com.csc.gdn.ipos.drive.api.device.model.Device;
import com.csc.gdn.ipos.drive.api.model.DriveRequest;
import com.csc.gdn.ipos.drive.api.model.OldDriveResponse;
import com.csc.gdn.ipos.drive.api.model.DriveResponse;
import com.csc.gdn.ipos.drive.storage.model.DriveStorageDataSource;


public class DriveBusinessServiceInterfaceImpl extends AbstractBusinessServiceInterface implements IBusinessServiceInterface {
	@Autowired
	private DriveStorageDataSource driveStorageDataSource;
	
	@Override
	public OldDriveResponse listFolderDocuments(RequestProfile requestProfile, String authToken, String folderId) throws ServiceProcessException {

		DriveRequest driveRequest = new DriveRequest();
		driveRequest.setParentFolderId(folderId);
		driveRequest.setRepositoryId(driveStorageDataSource.getRepositoryId());

		return doDriveService(authToken, "retrieveDocuments", requestProfile, driveRequest);

	}

	@Override
	public OldDriveResponse listFolderFolders(RequestProfile requestProfile, String authToken, String folderId) throws ServiceProcessException {

		DriveRequest driveRequest = new DriveRequest();
		// driveRequest.setRepositoryId(repositoryId);
		driveRequest.setParentFolderId(folderId);
		driveRequest.setRepositoryId(driveStorageDataSource.getRepositoryId());

		return doDriveService(authToken, "retrieveFolders", requestProfile, driveRequest);

	}

	@Override
	public OldDriveResponse retrieveDocumentByUUID(RequestProfile requestProfile, String authToken, String uuid) throws ServiceProcessException {

		DriveRequest driveRequest = new DriveRequest();
		driveRequest.setUuid(uuid);
		driveRequest.setGroupId(driveStorageDataSource.getGroupId());

		return doDriveService(authToken, "retrieveDocumentByUUID", requestProfile, driveRequest);

	}

	@Override
	public OldDriveResponse removeAllDocument(RequestProfile requestProfile, String authToken, String parentFolderId) throws ServiceProcessException {

		DriveRequest driveRequest = new DriveRequest();
		driveRequest.setRepositoryId(driveStorageDataSource.getRepositoryId());
		driveRequest.setParentFolderId(parentFolderId);

		return doDriveService(authToken, "deleteAllChildDocument", requestProfile, driveRequest);
	}

	@Override
	public OldDriveResponse removeAllFolder(RequestProfile requestProfile, String authToken, String parentFolderId) throws ServiceProcessException {

		DriveRequest driveRequest = new DriveRequest();
		driveRequest.setRepositoryId(driveStorageDataSource.getRepositoryId());
		driveRequest.setParentFolderId(parentFolderId);

		return doDriveService(authToken, "deleteAllChildFolder", requestProfile, driveRequest);

	}

	@Override
	public OldDriveResponse retrieveDocumentByFileEntryId(RequestProfile requestProfile, String authToken, String fileEntryId) throws ServiceProcessException {

		DriveRequest driveRequest = new DriveRequest();
		driveRequest.setRepositoryId(driveStorageDataSource.getRepositoryId());
		driveRequest.setFileEntryId(fileEntryId);

		return doDriveService(authToken, "retrieveDocumentByFileEntryId", requestProfile, driveRequest);

	}

	@Override
	public OldDriveResponse saveDocument(RequestProfile requestProfile, String authToken, String parentFolderId, String description, String changeLog, File file) throws ServiceProcessException {

		DriveRequest driveRequest = new DriveRequest();
		driveRequest.setRepositoryId(driveStorageDataSource.getRepositoryId());
		driveRequest.setParentFolderId(parentFolderId);
		driveRequest.setDescription(description);
		driveRequest.setChangeLog(changeLog);
		driveRequest.setDocumentFile(file);

		return doDriveService(authToken, "saveDocument", requestProfile, driveRequest);

	}

	@Override
	public OldDriveResponse saveDocumentWithMetaData(RequestProfile requestProfile, String authToken, String parentFolderId,  String description, String changeLog, String jsonMetaData, boolean approved, File file) throws ServiceProcessException {
		
		DriveRequest driveRequest = new DriveRequest();
		driveRequest.setGroupId(driveStorageDataSource.getGroupId());
		driveRequest.setRepositoryId(driveStorageDataSource.getRepositoryId());
		driveRequest.setParentFolderId(parentFolderId);
		driveRequest.setDescription(description);
		driveRequest.setChangeLog(changeLog);
		driveRequest.setDocumentMetaDataJSON(jsonMetaData);
		driveRequest.setApproved(approved);
		driveRequest.setDocumentFile(file);

		return doDriveService(authToken, "saveDocumentWithMetaData", requestProfile, driveRequest);
	}
	
	@Override
	public OldDriveResponse updateDocumentWithMetaData(RequestProfile requestProfile, String authToken, String uuid, String description, String changeLog, boolean majorVersion, String fileTypeId, String jsonMetaData, File file) throws ServiceProcessException {
		// TODO Auto-generated method stub
		DriveRequest driveRequest = new DriveRequest();
		driveRequest.setUuid(uuid);
		driveRequest.setGroupId(driveStorageDataSource.getGroupId());
		driveRequest.setDescription(description);
		driveRequest.setChangeLog(changeLog);
		driveRequest.setMajorVersion(majorVersion);
		driveRequest.setDocumentMetaDataJSON(jsonMetaData);
		driveRequest.setDocumentFile(file);
		driveRequest.setDocumentTypeId(fileTypeId);

		return doDriveService(authToken, "updateDocumentWithMetaData", requestProfile, driveRequest);
	}

	@Override
	public OldDriveResponse retrieveDocumentByUUIDBinary(RequestProfile requestProfile, String authToken, String uuid) throws ServiceProcessException {
		// TODO Auto-generated method stub
		DriveRequest driveRequest = new DriveRequest();
		driveRequest.setUuid(uuid);
		driveRequest.setGroupId(driveStorageDataSource.getGroupId());

		return doDriveService(authToken, "retrieveDocumentByUUIDBinary", requestProfile, driveRequest);
	}

	
	@Override
	public OldDriveResponse getDocumentFileEntryByUUID(RequestProfile requestProfile, String authToken, String uuid) throws ServiceProcessException {
		// TODO Auto-generated method stub
		DriveRequest driveRequest = new DriveRequest();
		driveRequest.setUuid(uuid);
		driveRequest.setGroupId(driveStorageDataSource.getGroupId());
		
		return doDriveService(authToken, "getDocumentFileEntryByUUID", requestProfile, driveRequest);
	}

	@Override
	public OldDriveResponse getDocumentByFileEntry(RequestProfile requestProfile, String authToken, String fileEntryId) throws ServiceProcessException {
		// TODO Auto-generated method stub
		DriveRequest driveRequest = new DriveRequest();
		driveRequest.setFileEntryId(fileEntryId);
		return doDriveService(authToken, "getDocumentByFileEntry", requestProfile, driveRequest);
	}

	@Override
	public OldDriveResponse copyDocument(RequestProfile requestProfile,
			String authToken, String fileEntryId, String desFolderId)
			throws ServiceProcessException {
		DriveRequest driveRequest = new DriveRequest();
		driveRequest.setGroupId(driveStorageDataSource.getGroupId());
		driveRequest.setRepositoryId(driveStorageDataSource.getRepositoryId());
		driveRequest.setFileEntryId(fileEntryId);
		driveRequest.setParentFolderId(desFolderId);
		return doDriveService(authToken, "copyDocument", requestProfile, driveRequest);
	}

	@Override
	public OldDriveResponse getDocumentMetaData(RequestProfile requestProfile, String authToken, Map<String, String> keywords) throws ServiceProcessException {
		// TODO Auto-generated method stub
		DriveRequest driveRequest = new DriveRequest();
		driveRequest.setGroupId(driveStorageDataSource.getGroupId());
		driveRequest.setKeywords(keywords);
		return doDriveService(authToken, "getDocumentMetaData", requestProfile, driveRequest);
	}

	@Override
	public OldDriveResponse copyFolder(RequestProfile requestProfile,
			String authToken, String sourceFolderId, String parentFolderId,
			String name, String description) throws ServiceProcessException {
		// TODO Auto-generated method stub
		DriveRequest driveRequest = new DriveRequest();
		driveRequest.setRepositoryId(driveStorageDataSource.getRepositoryId());
		driveRequest.setSourceFolderId(sourceFolderId);
		driveRequest.setParentFolderId(parentFolderId);
		driveRequest.setFolderName(name);
		driveRequest.setDescription(description);
		return doDriveService(authToken, "copyFolder", requestProfile, driveRequest);
	}

	@Override
	public OldDriveResponse getDocumentMetaDataByPath(
			RequestProfile requestProfile, String authToken, String path,
			String fileName) throws ServiceProcessException {
		// TODO Auto-generated method stub
		DriveRequest driveRequest = new DriveRequest();
		driveRequest.setGroupId(driveStorageDataSource.getGroupId());
		driveRequest.setPath(path);
		driveRequest.setFileName(fileName);
		return doDriveService(authToken, "getDocumentMetaDataByPath", requestProfile, driveRequest);
	}
	@Override
	public OldDriveResponse createDocumentMetaData(RequestProfile requestProfile,
			String authToken, String name, String description, String listFields)
			throws ServiceProcessException {
		// TODO Auto-generated method stub
		DriveRequest driveRequest = new DriveRequest();
		driveRequest.setGroupId(driveStorageDataSource.getGroupId());
		driveRequest.setStructureName(name);
		driveRequest.setDescription(description);
		driveRequest.setListFieldsJson(listFields);
		return doDriveService(authToken, "createDocumentMetaData", requestProfile, driveRequest);
	}	@Override
	public OldDriveResponse checkFolderExistByName(RequestProfile requestProfile,
			String authToken, String parentFolderId, String folderName)
			throws ServiceProcessException {

		DriveRequest driveRequest = new DriveRequest();
		driveRequest.setRepositoryId(driveStorageDataSource.getRepositoryId());
		driveRequest.setParentFolderId(parentFolderId);
		driveRequest.setFolderName(folderName);
		return doDriveService(authToken, "checkFolderExistByName", requestProfile, driveRequest);
		
	}

	@Override
	public OldDriveResponse addFolder(RequestProfile requestProfile,
			String authToken, String parentFolderId, String folderName)
			throws ServiceProcessException {
		
		DriveRequest driveRequest = new DriveRequest();
		driveRequest.setRepositoryId(driveStorageDataSource.getRepositoryId());
		driveRequest.setParentFolderId(parentFolderId);
		driveRequest.setFolderName(folderName);
		driveRequest.setDescription("add Folder");
		return doDriveService(authToken, "addFolder", requestProfile, driveRequest);
	}


	@Override
	public OldDriveResponse removeFileByFolderIdAndFileName(
			RequestProfile requestProfile, String authToken, String folderId,
			String fileName) throws ServiceProcessException {
		DriveRequest driveRequest = new DriveRequest();
		driveRequest.setGroupId(driveStorageDataSource.getGroupId());
		driveRequest.setParentFolderId(folderId);
		driveRequest.setFileName(fileName);
		
		OldDriveResponse driveResponse = new OldDriveResponse();
		driveResponse = doDriveService(authToken, "getFileEntryByFolderIdAndFileName", requestProfile, driveRequest);
		String result = (String) driveResponse.getResult();
		Object obj = JSONValue.parse(result);
		JSONArray jsonArray = (JSONArray) obj;
		JSONObject jObj = (JSONObject)jsonArray.get(0);
		String fileEntryId = jObj.get("fileEntryId").toString().trim();
		
		driveRequest = new DriveRequest();
		driveRequest.setFileEntryId(fileEntryId);
		return doDriveService(authToken, "deleteDocumentByFileEntryId", requestProfile, driveRequest);
	}	
	public OldDriveResponse saveDocumentByPath(RequestProfile requestProfile, String authToken, String path, String description, String changeLog, File tempFile) throws ServiceProcessException {
		// TODO Auto-generated method stub
		DriveRequest driveRequest = new DriveRequest();
		driveRequest.setRepositoryId(driveStorageDataSource.getRepositoryId());
		driveRequest.setGroupId(driveStorageDataSource.getGroupId());
		driveRequest.setPath(path);
		driveRequest.setDescription(description);
		driveRequest.setChangeLog(changeLog);
		driveRequest.setDocumentFile(tempFile);

		return doDriveService(authToken, "saveDocumentByPath", requestProfile, driveRequest);
	}

	@Override
	public OldDriveResponse saveDocumentWithMetaDataByPath(RequestProfile requestProfile, String authToken, String path, String description, String changeLog,String fileTypeId, String jsonMetaData, boolean approved, File file) throws ServiceProcessException {
		// TODO Auto-generated method stub
		DriveRequest driveRequest = new DriveRequest();
		driveRequest.setGroupId(driveStorageDataSource.getGroupId());
		driveRequest.setRepositoryId(driveStorageDataSource.getRepositoryId());
		driveRequest.setPath(path);
		driveRequest.setDescription(description);
		driveRequest.setChangeLog(changeLog);
		driveRequest.setDocumentMetaDataJSON(jsonMetaData);
		driveRequest.setApproved(approved);
		driveRequest.setDocumentFile(file);
		driveRequest.setDocumentTypeId(fileTypeId);

		return doDriveService(authToken, "saveDocumentWithMetaDataByPath", requestProfile, driveRequest);

	}

	@Override
	public OldDriveResponse retrieveDocumentByFolderIdAndFileName(
			RequestProfile requestProfile, String authToken, String folderId,
			String fileName) throws ServiceProcessException {
		
		DriveRequest driveRequest = new DriveRequest();
		driveRequest.setGroupId(driveStorageDataSource.getGroupId());
		driveRequest.setParentFolderId(folderId);
		driveRequest.setFileName(fileName);
		
		OldDriveResponse driveResponse = new OldDriveResponse();
		driveResponse = doDriveService(authToken, "retrieveDocumentByFolderIdAndFileName", requestProfile, driveRequest);
		return driveResponse;
	}

	@Override
	public OldDriveResponse removeFolder(RequestProfile requestProfile,
			String authToken, String folderId) throws ServiceProcessException {
		// TODO Auto-generated method stub
		DriveRequest driveRequest = new DriveRequest();
		driveRequest.setFolderId(folderId);

		return doDriveService(authToken, "deleteFolder", requestProfile, driveRequest);
	}

	@Override
	public OldDriveResponse getAllDocument(RequestProfile requestProfile,
			String authToken) throws ServiceProcessException {
		// TODO Auto-generated method stub
		DriveRequest driveRequest = new DriveRequest();
		return doDriveService(authToken, "getAllDocument", requestProfile, driveRequest);
	}

	@Override
	public OldDriveResponse exampleSecondDMS(RequestProfile requestProfile,
			String authToken) throws ServiceProcessException {
		// TODO Auto-generated method stub
		DriveRequest driveRequest = new DriveRequest();
		return doDriveService(authToken, "exampleSecondDMS", requestProfile, driveRequest);
	}

	@Override
	public OldDriveResponse updateDocumentMetaData(RequestProfile requestProfile,
			String authToken, String name, String description, String listFields)
			throws ServiceProcessException {
		DriveRequest driveRequest = new DriveRequest();
		driveRequest.setGroupId(driveStorageDataSource.getGroupId());
		driveRequest.setStructureName(name);
		driveRequest.setDescription(description);
		driveRequest.setListFieldsJson(listFields);
		return doDriveService(authToken, "updateDocumentMetaData", requestProfile, driveRequest);
	}
	
	@Override
	public OldDriveResponse retrieveDocumentWithMetaData(
			RequestProfile requestProfile, String authToken, String uuid)
			throws ServiceProcessException {
		// TODO Auto-generated method stub
		DriveRequest driveRequest = new DriveRequest();
		driveRequest.setGroupId(driveStorageDataSource.getGroupId());
		driveRequest.setUuid(uuid);
		return doDriveService(authToken, "retrieveDocumentWithMetaData", requestProfile, driveRequest);
	}
	@Override
	public OldDriveResponse getAllDocumentOfFolderByPath(
			RequestProfile requestProfile, String authToken, String path)
			throws ServiceProcessException {
		// TODO Auto-generated method stub
		DriveRequest driveRequest = new DriveRequest();
		driveRequest.setGroupId(driveStorageDataSource.getGroupId());
		driveRequest.setPath(path);
		
		return doDriveService(authToken, "getAllDocumentOfFolderByPath", requestProfile, driveRequest);

	}
	@Override
	public OldDriveResponse checkDocumentExistByName(
			RequestProfile requestProfile, String authToken,
			String parentFolderId, String title) throws ServiceProcessException {
		DriveRequest driveRequest = new DriveRequest();
		driveRequest.setGroupId(driveStorageDataSource.getGroupId());
		driveRequest.setParentFolderId(parentFolderId);
		driveRequest.setFileName(title);
		return doDriveService(authToken, "checkDocumentExistByNameAndFolderId", requestProfile, driveRequest);
	}

	@Override
	public OldDriveResponse getMetaDatasByListUuid(RequestProfile requestProfile,
			String authToken, String listUuid) throws ServiceProcessException {
		DriveRequest driveRequest = new DriveRequest();
		driveRequest.setGroupId(driveStorageDataSource.getGroupId());
		driveRequest.setListFieldsJson(listUuid);
		return doDriveService(authToken, "getMetaDatasByListUuid", requestProfile, driveRequest);
	}

	@Override
	public OldDriveResponse getAllDocumentVersion(RequestProfile requestProfile,
			String authToken, String uuid) throws ServiceProcessException {
		DriveRequest driveRequest = new DriveRequest();
		driveRequest.setGroupId(driveStorageDataSource.getGroupId());
		driveRequest.setUuid(uuid);
		return doDriveService(authToken, "getAllDocumentVersion", requestProfile, driveRequest);
	}
	@Override
	public OldDriveResponse removeDocumentByUUID(RequestProfile requestProfile,
			String authToken, String uuid) throws ServiceProcessException {
		// TODO Auto-generated method stub
		DriveRequest driveRequest = new DriveRequest();
		driveRequest.setUuid(uuid);
		driveRequest.setGroupId(driveStorageDataSource.getGroupId());

		return doDriveService(authToken, "removeDocumentByUUID", requestProfile, driveRequest);
	}

	@Override
	public OldDriveResponse insertProcessAuditData(RequestProfile requestProfile,
			List<ProcessAuditData> processAuditData) throws ServiceProcessException {
		// TODO Auto-generated method stub
		DriveRequest driveRequest = new DriveRequest();
		driveRequest.setProcessAuditData(processAuditData);		

		return doMySQLService("mysql_InsertProcessAuditData", requestProfile, driveRequest);
	}

	@Override
	public OldDriveResponse insertTaskAuditData(RequestProfile requestProfile,
			List<TaskAuditData> taskAuditData) throws ServiceProcessException {
		// TODO Auto-generated method stub
		DriveRequest driveRequest = new DriveRequest();
		driveRequest.setTaskAuditData(taskAuditData);		

		return doMySQLService("mysql_InsertTaskAuditData", requestProfile, driveRequest);
	}

	@Override
	public OldDriveResponse insertUserAuditData(RequestProfile requestProfile,
			List<UserAuditData> userAuditData) throws ServiceProcessException {
		// TODO Auto-generated method stub
		DriveRequest driveRequest = new DriveRequest();
		driveRequest.setUserAuditData(userAuditData);		

		return doMySQLService("mysql_InsertUserAuditData", requestProfile, driveRequest);
	}

	@Override
	public OldDriveResponse selectAllProcessAuditData(RequestProfile requestProfile)
			throws ServiceProcessException {
		// TODO Auto-generated method stub
		DriveRequest driveRequest = new DriveRequest();	

		return doMySQLService("mysql_SelectAllProcessAuditData", requestProfile, driveRequest);
	}

	@Override
	public OldDriveResponse updateDocument(RequestProfile requestProfile,
			String authToken, String uuid, String description,
			String changeLog, boolean majorVersion, File file)
			throws ServiceProcessException {
		// TODO Auto-generated method stub
		DriveRequest driveRequest = new DriveRequest();
		driveRequest.setUuid(uuid);
		driveRequest.setGroupId(driveStorageDataSource.getGroupId());
		driveRequest.setDescription(description);
		driveRequest.setChangeLog(changeLog);
		driveRequest.setMajorVersion(majorVersion);
		driveRequest.setDocumentFile(file);
		return doDriveService(authToken, "updateDocument", requestProfile, driveRequest);
	}

	@Override
	public OldDriveResponse retrieveDocumentByUuidAndVersion(
			RequestProfile requestProfile, String authToken, String uuid,
			String version) throws ServiceProcessException {
		DriveRequest driveRequest = new DriveRequest();
		driveRequest.setUuid(uuid);
		driveRequest.setGroupId(driveStorageDataSource.getGroupId());
		driveRequest.setDocumentVersion(version);
		return doDriveService(authToken, "retrieveDocumentByUuidAndVersion", requestProfile, driveRequest);
	}

	@Override
	public OldDriveResponse insertAuditData(RequestProfile requestProfile,
			AuditData auditData) throws ServiceProcessException {
		// TODO Auto-generated method stub
		DriveRequest driveRequest = new DriveRequest();
		driveRequest.setAuditData(auditData);
		return doMySQLService("mysql_InsertAuditData", requestProfile, driveRequest);
	}

	@Override
	public OldDriveResponse deleteDocumentType(RequestProfile requestProfile,
			String authToken, String fileTypeId) throws ServiceProcessException {
		DriveRequest driveRequest = new DriveRequest();
		driveRequest.setDocumentTypeId(fileTypeId);

		return doDriveService(authToken, "deleteDocumentType", requestProfile, driveRequest);
	}
	@Override
	public OldDriveResponse insertUserData(RequestProfile requestProfile,
			String data) throws ServiceProcessException {
		// TODO Auto-generated method stub
		DriveRequest driveRequest = new DriveRequest();
		driveRequest.setDescription(data);
		return doMySQLService("mysql_Portal_InsertUserData", requestProfile, driveRequest);
	}

	@Override
	public OldDriveResponse deleteUserData(RequestProfile requestProfile,
			String data) throws ServiceProcessException {
		DriveRequest driveRequest = new DriveRequest();
		driveRequest.setDescription(data);
		return doMySQLService("mysql_Portal_DeleteUserData", requestProfile, driveRequest);
	}
	@Override
	public DriveResponse insertDeviceData(RequestProfile requestProfile,
			Device device) throws ServiceProcessException {
		DriveRequest driveRequest = new DriveRequest();
		driveRequest.setDevice(device);
		return doNewMySQLService("mysql_Portal_InsertDeviceData", requestProfile, driveRequest);
	}
	
	@Override
	public DriveResponse insertAppDeviceData(RequestProfile requestProfile, Device device,
			AppDevice appDevice) throws ServiceProcessException {
		DriveRequest driveRequest = new DriveRequest();
		driveRequest.setDevice(device);
		driveRequest.setAppDevice(appDevice);
		return doNewMySQLService("mysql_Portal_InsertAppDeviceData", requestProfile, driveRequest);
	}
	
	@Override
	public DriveResponse insertNotification(RequestProfile requestProfile,
			AppNotification appNotification) throws ServiceProcessException {
		DriveRequest driveRequest = new DriveRequest();
		driveRequest.setAppNotification(appNotification);
		return doNewMySQLService("mysql_Portal_InsertNotificationData", requestProfile, driveRequest);
	}

	@Override
	public OldDriveResponse moveFileEntry(RequestProfile requestProfile, String authToken,
			String path, String fileName, String targetFolderId)
			throws ServiceProcessException {
		DriveRequest driveRequest = new DriveRequest();
		driveRequest.setPath(path);
		driveRequest.setFileName(fileName);
		driveRequest.setFolderId(targetFolderId);
		driveRequest.setGroupId(driveStorageDataSource.getGroupId());
		return doDriveService(authToken, "moveFileEntry", requestProfile, driveRequest);
	}

	@Override
	public DriveResponse getLatestVersion(RequestProfile requestProfile,
			String authToken, String uuid) throws ServiceProcessException {
		DriveRequest driveRequest = new DriveRequest();
		driveRequest.setUuid(uuid);
		driveRequest.setGroupId(driveStorageDataSource.getGroupId());
		return doDriveStorage(authToken, "getLatestVersion", requestProfile, driveRequest);
	}

	@Override
	public DriveResponse moveFolder(RequestProfile requestProfile,
			String authToken, String folderId, String targetParentFolderId)
			throws ServiceProcessException {
		DriveRequest driveRequest = new DriveRequest();
		driveRequest.setFolderId(folderId);
		driveRequest.setParentFolderId(targetParentFolderId);
		driveRequest.setGroupId(driveStorageDataSource.getGroupId());
		return doDriveStorage(authToken, "moveFolder", requestProfile,
				driveRequest);
	}

	@Override
	public DriveResponse getMetaDataByVersion(RequestProfile requestProfile,
			String authToken, String uuid, String version)
			throws ServiceProcessException {
		DriveRequest driveRequest = new DriveRequest();
		driveRequest.setUuid(uuid);
		driveRequest.setDocumentVersion(version);
		driveRequest.setGroupId(driveStorageDataSource.getGroupId());
		return doDriveStorage(authToken, "getMetaDataByVersion", requestProfile,
				driveRequest);
	}

	@Override
	public DriveResponse removeNotification(RequestProfile requestProfile,
			AppNotification appNotification) throws ServiceProcessException {
		DriveRequest driveRequest = new DriveRequest();
		driveRequest.setAppNotification(appNotification);
		return doNewMySQLService("mysql_Portal_RemoveNotificationData", requestProfile, driveRequest);
	}
}