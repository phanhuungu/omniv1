package com.csc.gdn.ipos.drive.web.bsi;

import java.io.File;
import java.util.List;
import java.util.Map;

import com.csc.gdn.ipos.core.exception.ServiceProcessException;
import com.csc.gdn.ipos.core.profile.RequestProfile;
import com.csc.gdn.ipos.drive.api.audit.model.AuditData;
import com.csc.gdn.ipos.drive.api.audit.model.ProcessAuditData;
import com.csc.gdn.ipos.drive.api.audit.model.TaskAuditData;
import com.csc.gdn.ipos.drive.api.audit.model.UserAuditData;
import com.csc.gdn.ipos.drive.api.device.model.AppDevice;
import com.csc.gdn.ipos.drive.api.device.model.AppNotification;
import com.csc.gdn.ipos.drive.api.device.model.Device;
import com.csc.gdn.ipos.drive.api.model.OldDriveResponse;
import com.csc.gdn.ipos.drive.api.model.DriveResponse;

public interface IBusinessServiceInterface extends IBaseBusinessServiceInterface {

	public OldDriveResponse getDriveSession(RequestProfile requestProfile, String username, String password)  throws ServiceProcessException;

	public OldDriveResponse listFolderDocuments(RequestProfile requestProfile, String authToken, String folderId)  throws ServiceProcessException;

	public OldDriveResponse listFolderFolders(RequestProfile requestProfile, String authToken, String folderId)  throws ServiceProcessException;

	public OldDriveResponse retrieveDocumentByUUID(RequestProfile requestProfile, String authToken, String uuid)  throws ServiceProcessException;
	
	public OldDriveResponse removeAllDocument(RequestProfile requestProfile, String authToken, String parentFolderId)  throws ServiceProcessException;
	
	public OldDriveResponse removeAllFolder(RequestProfile requestProfile, String authToken, String parentFolderId)  throws ServiceProcessException;
	
	public OldDriveResponse removeFolder(RequestProfile requestProfile, String authToken, String folderId)  throws ServiceProcessException;

	public OldDriveResponse retrieveDocumentByFileEntryId(RequestProfile requestProfile, String authToken, String fileEntryId)  throws ServiceProcessException;

	public OldDriveResponse saveDocument(RequestProfile requestProfile, String authToken, String parentFolderId, String description, String changeLog, File file)  throws ServiceProcessException;

	public OldDriveResponse saveDocumentWithMetaData(RequestProfile requestProfile, String authToken, String parentFolderId, String description, String changeLog, String jsonMetaData, boolean approved, File file)  throws ServiceProcessException;

	public OldDriveResponse updateDocumentWithMetaData(RequestProfile requestProfile, String authToken, String uuid, String description, String changeLog, boolean majorVersion, String fileTypeId, String jsonMetaData, File file)  throws ServiceProcessException;

	public OldDriveResponse retrieveDocumentByUUIDBinary(RequestProfile requestProfile, String d_authToken, String uuid)  throws ServiceProcessException;

	public OldDriveResponse getDocumentFileEntryByUUID(RequestProfile requestProfile, String authToken, String uuid)  throws ServiceProcessException;

	public OldDriveResponse getDocumentByFileEntry(RequestProfile requestProfile, String d_authToken, String fileentry)  throws ServiceProcessException;
	
	public OldDriveResponse copyDocument(RequestProfile requestProfile, String authToken, String fileEntryId, String destFolderId)  throws ServiceProcessException;

	public OldDriveResponse copyFolder(RequestProfile requestProfile, String authToken, String sourceFolderId, String parentFolderId, String name, String description)  throws ServiceProcessException;

	public OldDriveResponse getDocumentMetaData(RequestProfile requestProfile, String authToken, Map<String, String> keywords) throws ServiceProcessException;

	public OldDriveResponse getDocumentMetaDataByPath(RequestProfile requestProfile, String authToken, String path, String fileName) throws ServiceProcessException;

	public OldDriveResponse createDocumentMetaData(RequestProfile requestProfile, String authToken, String name, String description, String listFields) throws ServiceProcessException;
	
	public OldDriveResponse saveDocumentByPath(RequestProfile requestProfile, String authToken, String path, String description,String changeLog, File tempFile) throws ServiceProcessException;
	
	public OldDriveResponse saveDocumentWithMetaDataByPath(RequestProfile requestProfile, String authToken, String path, String description, String changeLog, String fileTypeId, String jsonMetaData, boolean approved, File file)  throws ServiceProcessException;
	
	public OldDriveResponse checkFolderExistByName(RequestProfile requestProfile, String authToken, String parentFolderId, String folderName) throws ServiceProcessException;

	public OldDriveResponse addFolder(RequestProfile requestProfile, String authToken, String parentFolderId, String folderName) throws ServiceProcessException;
	
	public OldDriveResponse removeFileByFolderIdAndFileName(RequestProfile requestProfile, String authToken, String folderId, String fileName) throws ServiceProcessException;
	
	public OldDriveResponse retrieveDocumentByFolderIdAndFileName(RequestProfile requestProfile, String authToken, String folderId, String fileName) throws ServiceProcessException;

	public OldDriveResponse getAllDocument(RequestProfile requestProfile, String authToken) throws ServiceProcessException;
	
	public OldDriveResponse exampleSecondDMS(RequestProfile requestProfile, String authToken) throws ServiceProcessException;

	public OldDriveResponse retrieveDocumentWithMetaData(RequestProfile requestProfile, String authToken, String uuid)  throws ServiceProcessException;

	public OldDriveResponse checkDocumentExistByName(RequestProfile requestProfile, String authToken, String parentFolderId, String title) throws ServiceProcessException;

	public OldDriveResponse updateDocumentMetaData(RequestProfile requestProfile, String authToken, String name, String description, String listFields) throws ServiceProcessException;

	public OldDriveResponse getAllDocumentOfFolderByPath(RequestProfile requestProfile, String authToken, String path) throws ServiceProcessException;
	
	public OldDriveResponse getMetaDatasByListUuid(RequestProfile requestProfile, String authToken, String listUuid) throws ServiceProcessException;

	public OldDriveResponse getAllDocumentVersion(RequestProfile requestProfile, String authToken, String uuid) throws ServiceProcessException;	public OldDriveResponse removeDocumentByUUID(RequestProfile requestProfile, String authToken, String uuid) throws ServiceProcessException;
	
	public OldDriveResponse insertProcessAuditData(RequestProfile requestProfile, List<ProcessAuditData> processAuditData) throws ServiceProcessException;

	public OldDriveResponse insertTaskAuditData(RequestProfile requestProfile, List<TaskAuditData> taskAuditData) throws ServiceProcessException;
	
	public OldDriveResponse insertUserAuditData(RequestProfile requestProfile, List<UserAuditData> userAuditData) throws ServiceProcessException;
	
	public OldDriveResponse selectAllProcessAuditData(RequestProfile requestProfile) throws ServiceProcessException;

	public OldDriveResponse updateDocument(RequestProfile requestProfile, String authToken, String uuid, String description, String changeLog, boolean majorVersion, File file)  throws ServiceProcessException;
	
	public OldDriveResponse retrieveDocumentByUuidAndVersion(RequestProfile requestProfile, String authToken, String uuid, String version)  throws ServiceProcessException;	

	public OldDriveResponse insertAuditData(RequestProfile requestProfile, AuditData auditData) throws ServiceProcessException;

	public OldDriveResponse deleteDocumentType(RequestProfile requestProfile, String authToken, String fileTypeId)  throws ServiceProcessException;
	
	public OldDriveResponse getDriveSessionForRuntime(RequestProfile requestProfile, String username, String password) throws ServiceProcessException;

	public OldDriveResponse insertUserData(RequestProfile requestProfile, String data) throws ServiceProcessException;

	public OldDriveResponse deleteUserData(RequestProfile requestProfile, String data) throws ServiceProcessException;
	
	public DriveResponse insertDeviceData(RequestProfile requestProfile, Device device) throws ServiceProcessException;
	
	public DriveResponse insertAppDeviceData(RequestProfile requestProfile, Device device, AppDevice appDevice) throws ServiceProcessException;

	public DriveResponse insertNotification(RequestProfile requestProfile, AppNotification appNotification) throws ServiceProcessException;

	public DriveResponse removeNotification(RequestProfile requestProfile, AppNotification appNotification) throws ServiceProcessException;

	public OldDriveResponse moveFileEntry(RequestProfile requestProfile, String authToken, String path, String fileName, String targetFolderId) throws ServiceProcessException;

	public DriveResponse getLatestVersion(RequestProfile requestProfile, String authToken, String uuid) throws ServiceProcessException;

	public DriveResponse moveFolder(RequestProfile requestProfile, String authToken, String folderId, String targetParentFolderId) throws ServiceProcessException;

	public DriveResponse getMetaDataByVersion(RequestProfile requestProfile, String authToken, String uuid, String version) throws ServiceProcessException;
}
