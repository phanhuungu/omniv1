package com.csc.gdn.ipos.drive.web.model;


public class InternalDriveSession {
	private String driveAuthToken;
	private String username;
	private String password;
	private long lastActiveTimestamp;
	private int counter;
	private String platform;
	
	public static String RUNTIME = "runtime";
	public static String IOS = "ios";
	
	public String getDriveAuthToken() {
		return driveAuthToken;
	}
	public void setDriveAuthToken(String driveAuthToken) {
		this.driveAuthToken = driveAuthToken;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public long getLastActiveTimestamp() {
		return lastActiveTimestamp;
	}
	public void setLastActiveTimestamp(long lastActiveTimestamp) {
		this.lastActiveTimestamp = lastActiveTimestamp;
	}
	public int getCounter() {
		return counter;
	}
	public void setCounter(int counter) {
		this.counter = counter;
	}
	public String getPlatform() {
		return platform;
	}
	public void setPlatform(String platform) {
		this.platform = platform;
	}

}
